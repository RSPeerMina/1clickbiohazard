package main;

import org.rspeer.runetek.api.Varps;
import org.rspeer.runetek.api.commons.StopWatch;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.Dialog;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.runetek.event.listeners.RenderListener;
import org.rspeer.runetek.event.types.RenderEvent;
import org.rspeer.script.Script;
import org.rspeer.script.ScriptCategory;
import org.rspeer.script.ScriptMeta;
import org.rspeer.ui.Log;

import java.awt.*;

import static quests.Biohazard.biohazard;
import static quests.PlagueCity.plagueCity;

@ScriptMeta(
        name = "1ClickBiohazard",
        desc = "Completes Plague city and Biohazard, read thread for info",
        developer = "Mina",
        category = ScriptCategory.QUESTING)

public class Quester extends Script implements RenderListener {

    private StopWatch timer;
    private String currentQuest;

    @Override
    public void onStart() {
        timer = StopWatch.start();
    }

    public static boolean isAnimating() {
        return Players.getLocal().isAnimating();
    }

    public int loop() {

        if (Movement.getRunEnergy() >= Random.low(15, 30) && !Movement.isRunEnabled()) {
            Movement.toggleRun(true);
        }

        if (Varps.get(165) != 30) {
            currentQuest = "Plague City";
            if (Varps.get(165) != 29) {
                plagueCity();
            } else {
                if (Inventory.contains(x -> x.getName().contains("scroll"))) {
                    if (org.rspeer.runetek.api.component.Dialog.canContinue()) {
                        Dialog.processContinue();
                    } else {
                        Inventory.getFirst(x -> x.getName().contains("scroll")).interact("Read");
                    }
                }
            }

        } else {
            if (Varps.get(68) != 16) {
                currentQuest = "Biohazard";
                biohazard();
            } else {
                Log.fine("Done");
                setStopping(true);
            }
        }


        return 500;
    }

    @Override
    public void notify(RenderEvent m) {
        Graphics g = m.getSource();
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        int y = 35;
        int x = 10;

        g2.setColor(new Color(0.0f, 0.0f, 0.0f, 0.5f));
        g2.fillRect(x - 5, y - 15, 150, 60);
        g2.setColor(Color.MAGENTA);
        g2.drawString("1ClickBiohazard by Mina", x, y);
        g2.drawString("Runtime: " + timer.toElapsedString(), x, y += 20);
        g2.drawString("Current quest: " + currentQuest, x, y += 20);
    }

    public void onStop() {
        Log.fine("Thank you for using 1ClickBiohazard by Mina");
        super.onStop();
    }
}
