package quests;

import org.rspeer.runetek.adapter.scene.Npc;
import org.rspeer.runetek.adapter.scene.Pickable;
import org.rspeer.runetek.adapter.scene.SceneObject;
import org.rspeer.runetek.api.Varps;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.Bank;
import org.rspeer.runetek.api.component.Dialog;
import org.rspeer.runetek.api.component.GrandExchange;
import org.rspeer.runetek.api.component.tab.EquipmentSlot;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.movement.position.Area;
import org.rspeer.runetek.api.movement.position.Position;
import org.rspeer.runetek.api.scene.Npcs;
import org.rspeer.runetek.api.scene.Pickables;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.runetek.api.scene.SceneObjects;
import org.rspeer.runetek.providers.RSGrandExchangeOffer;
import org.rspeer.ui.Log;


public class PlagueCity {
    private static int t = 1;
    private static Area EDMONDHOUSE = Area.rectangular(2563, 3329, 2579, 3338);
    private static Area TUNNEL = Area.rectangular(2509, 9737, 2519, 9762);
    private static Area CLERKROOM = Area.rectangular(2522, 3312, 2529, 3319);
    private static Area BRAVEKROOM = Area.rectangular(2530, 3312, 2539, 3316);
    private static Area MILLIHOUSE = Area.rectangular(2527, 3329, 2533, 3333);
    private static Area PLAGUEHOUSE = Area.rectangular(2532, 3268, 2541, 3272);
    private static Area GRANDEXCHANGE = Area.rectangular(3160, 3485, 3169, 3493);
    private static Area CELL = Area.rectangular(2540, 9669, 2542, 9673, 0);
    private static Position MANHOLE = new Position(2529, 3302);

    private static String[] REQITEM = {
            "Dwellberries",
            "Rope",
            "Spade",
            "Bucket of water",
            "Bucket of milk",
            "Chocolate dust",
            "Snape grass"
    };

    private static String[] dialogueOptions = {
            "The Outpost",
            "happened to her",
            "Can I help",
            "Yes",
            "looking for a woman",
            "return it for you",
            "a kidnap victim is in here",
            "I want to check",
            "Who is through that door",
            "This is urgent though",
            "This is really important though",
            "in the cure",
            "listen to me"
    };

    private static boolean isDialogueOption(String dialogue) {
        for (String option : dialogueOptions) {
            if (dialogue.contains(option)) return true;
        }
        return false;
    }

    private static boolean advanceDialogue() {
        if (Dialog.isOpen()) {
            Time.sleep(500, 1000);
            if (Dialog.canContinue()) {
                Dialog.processContinue();
                Time.sleep(200, 800);
            } else if (Dialog.process(s -> isDialogueOption(s))) {
                Time.sleep(200, 800);
            } else {
                Log.severe("Unknown dialogue");
            }
            return true;
        }
        return false;
    }

    private static boolean hasPicture() {
        return Inventory.contains("Picture");
    }

    private static boolean hasQuestItems() {
        return Inventory.contains("Dwellberries")
                && Inventory.contains("Rope")
                && Inventory.contains("Spade")
                && Inventory.getCount(false, "Bucket of water") >= 4
                && Inventory.contains("Bucket of milk")
                && Inventory.contains("Chocolate dust")
                && Inventory.contains("Snape grass");
    }

    private static void makeCure() {
        if (Inventory.contains("Chocolate dust") && Inventory.getFirst("Chocolate dust").interact("Use") && !Inventory.isItemSelected()) {
            Time.sleepUntil(Inventory::isItemSelected, 1000);
        }

        if (Inventory.isItemSelected() && Inventory.contains("Bucket of milk") && Inventory.getFirst("Bucket of milk").interact("Use")) {
            Time.sleep(500, 1500);
        }

        if (Inventory.contains("Snape grass") && Inventory.getFirst("Snape grass").interact("Use") && !Inventory.isItemSelected()) {
            Time.sleepUntil(Inventory::isItemSelected, 1000);
        }

        if (Inventory.isItemSelected() && Inventory.contains("Chocolatey milk") && Inventory.getFirst("Chocolatey milk").interact("Use")) {
            Time.sleep(500, 1500);
        }
    }

    private static void talkToEdmondInHouse() {
        if (EDMONDHOUSE.contains(Players.getLocal())) {
            Npc EDMOND = Npcs.getNearest("Edmond");

            if (EDMOND != null && EDMOND.isPositionInteractable()) {
                EDMOND.interact("Talk-to");
                Time.sleepUntil(Dialog::isViewingChat, 1000, 2000);
            }
        } else {
            if (EDMONDHOUSE.getCenter().distance() >= 300) {
                if (Inventory.contains(x -> x.getName().contains("passage"))) {
                    Inventory.getFirst(x -> x.getName().contains("passage")).interact("Rub");
                }
            } else {
                Movement.walkToRandomized(EDMONDHOUSE.getCenter());
                Time.sleep(1000, 2000);
            }
        }
    }

    private static void talkToAlrena() {
        if (EDMONDHOUSE.contains(Players.getLocal())) {
            Npc ALRENA = Npcs.getNearest("Alrena");

            if (ALRENA != null && ALRENA.isPositionInteractable()) {
                ALRENA.interact("Talk-to");
                Time.sleepUntil(Dialog::isViewingChat, Random.low(500, 800), Random.low(1200, 1500));
            }
        } else {
            if (EDMONDHOUSE.getCenter().distance() >= 300) {
                if (Inventory.contains(x -> x.getName().contains("passage"))) {
                    Inventory.getFirst(x -> x.getName().contains("passage")).interact("Rub");
                }
            } else {
                Movement.walkToRandomized(EDMONDHOUSE.getCenter());
                Time.sleep(1000, 2000);
            }
        }
    }

    private static void talkToMartha() {
        if (MILLIHOUSE.contains(Players.getLocal())) {
            Npc MARTHA = Npcs.getNearest("Martha Rehnison");

            if (MARTHA != null) {
                MARTHA.interact("Talk-to");
                Time.sleepUntil(Dialog::isViewingChat, Random.low(500, 800), Random.low(1200, 1500));
            }
        } else {
            Movement.walkTo(MILLIHOUSE.getCenter());
            Time.sleep(1000, 2000);
        }
    }

    private static void talkToMilli() {
        if (Players.getLocal().getFloorLevel() == 1) {
            Npc MILLI = Npcs.getNearest("Milli Rehnison");

            if (MILLI != null) {
                MILLI.interact("Talk-to");
                Time.sleepUntil(Dialog::isViewingChat, Random.low(500, 800), Random.low(1200, 1500));
            }
        } else {
            if (MILLIHOUSE.contains(Players.getLocal())) {
                if (Players.getLocal().getFloorLevel() != 1) {
                    SceneObject STAIRS = SceneObjects.getNearest("Stairs");

                    if (STAIRS != null) {
                        if (STAIRS.interact("Walk-up")) {
                            Time.sleepUntil(() -> Players.getLocal().getFloorLevel() == 1, 1000, 2000);
                        }
                    }
                }
            } else {
                Movement.walkTo(MILLIHOUSE.getCenter());
                Time.sleep(1000, 2000);
            }
        }
    }

    private static void talkToClerk() {
        if (CLERKROOM.contains(Players.getLocal())) {
            Npc CLERK = Npcs.getNearest(x -> x.getName().equals("Clerk") && x.isPositionInteractable());

            if (CLERK != null) {
                CLERK.interact("Talk-to");
                Time.sleepUntil(Dialog::isViewingChat, Random.low(500, 800), Random.low(1200, 1500));
            }
        } else {
            Movement.walkToRandomized(CLERKROOM.getCenter());
            Time.sleep(1000, 2000);
        }
    }

    private static void talkToBravek() {
        if (BRAVEKROOM.contains(Players.getLocal())) {
            Npc BRAVEK = Npcs.getNearest("Bravek");

            if (BRAVEK != null) {
                BRAVEK.interact("Talk-to");
                Time.sleepUntil(Dialog::isViewingChat, Random.low(500, 800), Random.low(1200, 1500));
            }
        } else {
            Movement.walkToRandomized(BRAVEKROOM.getCenter());
            Time.sleep(1000, 2000);
        }
    }

    public static void buyItems() {
        if (GRANDEXCHANGE.contains(Players.getLocal())) {
            if (Inventory.contains("Coins")) {
                if (GrandExchange.isOpen()) {
                    GrandExchange.createOffer(RSGrandExchangeOffer.Type.BUY);
                } else {
                    GrandExchange.open();
                    Time.sleepUntil(() -> GrandExchange.isOpen(), 1000, 1000);
                }
            } else {
                if (Bank.isOpen()) {
                    Bank.withdrawAll("Coins");
                } else {
                    Bank.open();
                }
            }
        } else {
            Movement.walkToRandomized(GRANDEXCHANGE.getCenter());
            Time.sleep(1000, 2000);
        }
    }

    public static void plagueCity() {
        if (Varps.get(165) != 29) {
            if (!advanceDialogue()) {
                switch (Varps.get(165)) {
                    case 0:
                        if (hasQuestItems()) {
                            talkToEdmondInHouse();
                        } else {
                            if (Bank.isOpen()) {
                                Bank.withdraw(REQITEM[0], 1);
                                Bank.withdraw(REQITEM[1], 1);
                                Bank.withdraw(REQITEM[2], 1);
                                Bank.withdraw(REQITEM[3], 4);
                                Bank.withdraw(REQITEM[4], 1);
                                Bank.withdraw(REQITEM[5], 1);
                                Bank.withdraw(REQITEM[6], 1);
                                Bank.withdraw(21146, 1);
                                Bank.withdraw(21149, 1);
                                Bank.withdraw(21151, 1);
                                Bank.withdraw(21153, 1);
                                Bank.withdraw(21155, 1);
                            } else {
                                Bank.open();
                            }
                        }

                        break;
                    case 1:
                        talkToAlrena();
                        break;
                    case 2:
                        if (EquipmentSlot.HEAD.getItem() == null || !EquipmentSlot.HEAD.getItem().getName().equals("Gas mask")) {
                            if (Inventory.contains("Gas mask")) {
                                Inventory.getFirst("Gas mask").interact("Wear");
                            }
                        } else {
                            if (hasPicture()) {
                                talkToEdmondInHouse();
                            } else {
                                Pickable PICTURE = Pickables.getNearest("Picture");

                                if (PICTURE != null && PICTURE.isPositionInteractable()) {
                                    PICTURE.interact("Take");
                                } else {
                                    if (!PICTURE.isPositionInteractable()) {
                                        Movement.walkTo(new Position(2576, 3333));
                                        Time.sleep(1000, 2000);
                                    }
                                }
                            }
                        }
                        break;
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                        SceneObject MUDPATCH = SceneObjects.getNearest(2532);

                        if (Inventory.contains("Bucket of water") && Inventory.getFirst("Bucket of water").interact("Use") && !Inventory.isItemSelected()) {
                            Time.sleepUntil(Inventory::isItemSelected, 1000);
                        }

                        if (Inventory.isItemSelected() && MUDPATCH != null && MUDPATCH.interact("Use")) {
                            Time.sleep(500, 1500);
                        }
                        break;
                    case 7:
                        SceneObject WETMUDPATCH = SceneObjects.getNearest(2532);
                        if (Inventory.contains("Spade") && Inventory.getFirst("Spade").interact("Use") && !Inventory.isItemSelected()) {
                            Time.sleepUntil(Inventory::isItemSelected, 1000);
                        }

                        if (Inventory.isItemSelected() && WETMUDPATCH != null && WETMUDPATCH.interact("Use")) {
                            Time.sleep(500, 1500);
                        }
                        break;
                    case 8:
                        if (TUNNEL.contains(Players.getLocal())) {

                            SceneObject GRILL = SceneObjects.getNearest("Grill");

                            if (GRILL != null) {
                                switch (t) {
                                    case 1:
                                        GRILL.interact("Open");
                                        Time.sleepUntil(Dialog::processContinue, 1000, 4000);
                                        if (Dialog.isViewingChat()) {
                                            t++;
                                        }
                                        break;
                                    case 2:
                                        if (Inventory.contains("Rope") && Inventory.getFirst("Rope").interact("Use") && !Inventory.isItemSelected()) {
                                            Time.sleepUntil(Inventory::isItemSelected, 1000);
                                        }

                                        if (Inventory.isItemSelected() && GRILL.interact("Use")) {
                                            Time.sleep(500, 1500);
                                        }

                                        if (Dialog.isViewingChat()) {
                                            t++;
                                        }
                                        break;

                                }
                            }
                        }
                        break;
                    case 9:
                        Npc EDMONDSEWER = Npcs.getNearest("Edmond");

                        if (EDMONDSEWER != null) {
                            EDMONDSEWER.interact("Talk-to");
                            Time.sleepUntil(Dialog::isViewingChat, 1000, 2000);
                        } else {
                            Movement.walkTo(new Position(2517, 9735));
                            Time.sleep(1000, 2000);
                        }
                        break;
                    case 10:
                        SceneObject PIPE = SceneObjects.getNearest("Pipe");

                        if (new Position(2529, 3304).distance() >= 100) {
                            if (PIPE != null && PIPE.interact("Climb-up")) {
                                Time.sleep(2000, 4000);
                            }
                        } else {
                            Npc JETHICK = Npcs.getNearest("Jethick");

                            if (JETHICK != null) {
                                JETHICK.interact("Talk-to");
                            }
                        }
                        break;
                    case 20:
                    case 21:
                        talkToMartha();
                        break;
                    case 22:
                        talkToMilli();
                        break;
                    case 23:
                        if (PLAGUEHOUSE.getCenter().distance() <= 5) {
                            SceneObject PLAGUEDOOR = SceneObjects.getNearest(x -> x.getName().equals("Door") && x.containsAction("Open"));

                            if (PLAGUEDOOR != null) {
                                PLAGUEDOOR.interact("Open");
                            }
                        } else {
                            Movement.walkToRandomized(PLAGUEHOUSE.getCenter());
                            Time.sleep(1000, 2000);
                        }
                        break;
                    case 24:
                        talkToClerk();
                        break;
                    case 25:
                        talkToBravek();
                        break;
                    case 26:
                        if (Inventory.contains("Hangover cure")) {
                            talkToBravek();
                        } else {
                            makeCure();
                        }
                        break;
                    case 27:
                        if (new Position(2539, 9672).distance() <= 20) {
                            if (CELL.contains(Players.getLocal())) {
                                Npc ELENA = Npcs.getNearest("Elena");

                                if (ELENA != null) {
                                    ELENA.interact("Talk-to");
                                }
                            } else {
                                SceneObject DOOR = SceneObjects.getNearest("Door");

                                if (DOOR != null) {
                                    DOOR.interact("Open");
                                }
                            }

                        } else {
                            if (PLAGUEHOUSE.contains(Players.getLocal())) {
                                SceneObject BARREL = SceneObjects.getNearest(2530);

                                if (Inventory.contains("A small key")) {
                                    SceneObject STAIRS = SceneObjects.getNearest("Spooky stairs");

                                    if (STAIRS != null) {
                                        STAIRS.interact("Walk-down");
                                    }
                                } else {
                                    if (BARREL != null) {
                                        BARREL.interact("Search");
                                        Time.sleepUntil(() -> Inventory.contains("A small key"), 1500);
                                    }
                                }
                            } else {
                                if (PLAGUEHOUSE.getCenter().distance() <= 5) {
                                    SceneObject PLAGUEDOOR = SceneObjects.getNearest(x -> x.getName().equals("Door") && x.containsAction("Open"));

                                    if (PLAGUEDOOR != null) {
                                        PLAGUEDOOR.interact("Open");
                                    }
                                } else {
                                    Movement.walkToRandomized(PLAGUEHOUSE.getCenter());
                                    Time.sleep(1000, 2000);
                                }
                            }
                        }
                        break;
                    case 28:
                        if (CELL.getCenter().distance() <= 10) {
                            SceneObject STAIRS = SceneObjects.getNearest("Spooky stairs");

                            if (STAIRS != null && STAIRS.isPositionInteractable()) {
                                STAIRS.interact("Walk-up");
                            } else {
                                Movement.walkTo(STAIRS);
                                Time.sleep(1000, 2000);
                            }
                        } else {
                            if (EDMONDHOUSE.contains(Players.getLocal())) {
                                Npc EDMONDEND = Npcs.getNearest("Edmond");

                                if (EDMONDEND != null && EDMONDEND.isPositionInteractable()) {
                                    EDMONDEND.interact("Talk-to");
                                    Time.sleepUntil(Dialog::canContinue, 1000, 2000);
                                }
                            } else {
                                if (new Position(2514, 9739).distance() <= 100) {
                                    if (new Position(2518, 9755).distance() <= 10) {
                                        SceneObject MUDPILE = SceneObjects.getNearest("Mud pile");

                                        if (MUDPILE != null) {
                                            MUDPILE.interact("Climb");
                                            Time.sleepUntil(() -> MUDPILE == null, 1000, 3000);
                                        }
                                    } else {
                                        Movement.walkTo(new Position(2518, 9755));
                                        Time.sleep(1000, 2000);
                                    }

                                } else {
                                    if (MANHOLE.distance() <= 5) {
                                        SceneObject MANHOLECOVER = SceneObjects.getNearest("Manhole");

                                        if (MANHOLECOVER.containsAction("Open")) {
                                            MANHOLECOVER.interact("Open");
                                        } else {
                                            if (MANHOLECOVER.containsAction("Climb-down")) {
                                                MANHOLECOVER.interact("Climb-down");
                                            }
                                        }
                                    } else {
                                        Movement.walkTo(MANHOLE);
                                        Time.sleep(1000, 2000);
                                    }
                                }
                            }

                        }
                        break;
                }
            }
        } else {
            Log.fine("Done");
        }
    }
}
