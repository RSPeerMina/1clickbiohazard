package quests;

import org.rspeer.runetek.adapter.scene.Npc;
import org.rspeer.runetek.adapter.scene.Pickable;
import org.rspeer.runetek.adapter.scene.SceneObject;
import org.rspeer.runetek.api.Varps;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.Bank;
import org.rspeer.runetek.api.component.Dialog;
import org.rspeer.runetek.api.component.tab.EquipmentSlot;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.component.tab.Magic;
import org.rspeer.runetek.api.component.tab.Spell;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.movement.position.Area;
import org.rspeer.runetek.api.movement.position.Position;
import org.rspeer.runetek.api.scene.Npcs;
import org.rspeer.runetek.api.scene.Pickables;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.runetek.api.scene.SceneObjects;
import org.rspeer.ui.Log;

import static main.Quester.isAnimating;

public class Biohazard {

    private static Area WESTARDY = Area.rectangular(2511, 3265, 2556, 3333, 0);
    private static Area CHEMISTHOUSE = Area.rectangular(2929, 3207, 2936, 3213);
    private static Area EABANK = Area.rectangular(2613, 3331, 2620, 3333);
    private static Area BAR = Area.rectangular(3266, 3388, 3274, 3391);
    private static Area GUIDORHOUSE = Area.rectangular(3278, 3381, 3282, 3384);
    private static Area GUIDORROOM = Area.rectangular(3283, 3381, 3285, 3382);
    private static Area FINALLOCATION = Area.rectangular(3264, 3380, 3287, 3407);
    private static Area MOURNERHOUSEBACKYARD = Area.rectangular(2542, 3328, 2555, 3333);
    private static Area MOURNERSTAIR = Area.rectangular(2542, 3324, 2546, 3327);
    private static Area DOCTORHOUSE = Area.rectangular(2515, 3270, 2518, 3276);
    private static Area ELENAHOUSE = Area.rectangular(2590, 3334, 2594, 3338);
    private static Area JERICOHOUSE = Area.rectangular(2609, 3321, 2619, 3328);
    private static Area FENCEJUMP = Area.rectangular(2555, 3263, 2560, 3278);
    private static Area CRATEROOM = Area.rectangular(2552, 3325, 2554, 3326, 1);
    private static Area CAMPFIRE = Area.rectangular(2926, 3216, 2932, 3224);
    private static Area KINGROOM = Area.rectangular(2575, 3292, 2579, 3294, 1);
    private static Position TOWERFENCE = new Position(2563, 3301);
    private static Position ROTTENAPPLE = new Position(2535, 3332);

    private static int o = 0;
    private static int m = 0;

    private static String[] dialogueOptions = {
            "Yes",
            "I'll try to retrieve it for you",
            "lets do it",
            "Yes I do",
            "Your quest",
            "It's ok, I'm Elena's friend",
            "I just need some touch paper for a guy called Guidor",
            "TEST",
            "I've come to ask your assistance in stopping a plague",
            "I've been sent by your old pupil Elena",
            "That's why Elena wanted you to do it",
            "The Outpost",
            "I don't understand.."
    };

    private static boolean isDialogueOption(String dialogue) {
        for (String option : dialogueOptions) {
            if (dialogue.contains(option)) return true;
        }
        return false;
    }

    private static boolean advanceDialogue() {
        if (Dialog.isOpen()) {
            Time.sleep(500, 1000);
            if (Dialog.canContinue()) {
                Dialog.processContinue();
                Time.sleep(200, 800);
            } else if (Dialog.process(s -> isDialogueOption(s))) {
                Time.sleep(200, 800);
            } else {
                Log.severe("Unknown dialogue");
            }
            return true;
        }
        return false;
    }

    private static void sleep() {
        Time.sleep(1000, 2000);
    }

    private static boolean hasPlague() {
        return Inventory.contains("Liquid honey")
                && Inventory.contains("Ethenea")
                && Inventory.contains("Sulphuric broline")
                && Inventory.contains("Plague sample");
    }

    private static boolean hasLH() {
        return Inventory.contains("Liquid honey");
    }

    private static boolean hasET() {
        return Inventory.contains("Ethenea");
    }

    private static boolean hasSB() {
        return Inventory.contains("Sulphuric broline");
    }

    private static boolean hasBottom() {
        return EquipmentSlot.LEGS.getItem() != null && EquipmentSlot.LEGS.getItem().getId() == 428;
    }

    private static boolean hasTop() {
        return EquipmentSlot.CHEST.getItem() != null && EquipmentSlot.CHEST.getItem().getId() == 426;
    }

    private static void traverseToRimmington() {
        if (CHEMISTHOUSE.getCenter().distance() >= 400) {
            if (!isAnimating()) {
                if (Magic.canCast(Spell.Modern.HOME_TELEPORT)) {
                    Magic.cast(Spell.Modern.HOME_TELEPORT);
                    Time.sleep(8000);
                }
            }
        } else {
            Movement.walkToRandomized(CHEMISTHOUSE.getCenter());
            sleep();
        }
    }

    private static void talkToElena() {
        if (ELENAHOUSE.contains(Players.getLocal())) {
            Npc ELENA = Npcs.getNearest("Elena");

            if (ELENA != null) {
                ELENA.interact("Talk-to");
                Time.sleepUntil(Dialog::isViewingChat, 1000, 3000);
            }
        } else {
            if (ELENAHOUSE.getCenter().distance() >= 180 && Inventory.contains(x -> x.getName().contains("passage"))) {
                Inventory.getFirst(x -> x.getName().contains("passage")).interact("Rub");
            } else {
                Movement.walkToRandomized(ELENAHOUSE.getCenter());
                sleep();
            }
        }
    }

    private static void talkToJerico() {
        if (JERICOHOUSE.contains(Players.getLocal())) {
            Npc JERICO = Npcs.getNearest("Jerico");

            if (JERICO != null && JERICO.isPositionInteractable()) {
                JERICO.interact("Talk-to");
                Time.sleepUntil(Dialog::isViewingChat, 1000, 3000);
            } else {
                Movement.walkTo(new Position(2612, 3324));
                sleep();
            }
        } else {
            if (ELENAHOUSE.getCenter().distance() < JERICOHOUSE.getCenter().distance()) {
                Movement.walkTo(new Position(2605, 3296));
                sleep();
            } else {
                Movement.walkToRandomized(JERICOHOUSE.getCenter());
                Time.sleep(2000, 6000);
            }
        }
    }

    private static void talkToOmart() {
        if (FENCEJUMP.contains(Players.getLocal())) {
            Npc OMART = Npcs.getNearest("Omart");

            if (OMART != null) {
                OMART.interact("Talk-to");
                Time.sleepUntil(Dialog::isViewingChat, 1000, 3000);
                o = 1;
            }
        } else {
            Movement.walkToRandomized(FENCEJUMP.getCenter());
            sleep();
        }
    }

    public static void biohazard() {
        if (Varps.get(68) != 16) {
            if (!advanceDialogue()) {
                switch (Varps.get(68)) {
                    case 0:
                        talkToElena();
                        break;
                    case 1:
                        talkToJerico();
                        break;
                    case 2:
                        if (Inventory.contains("Bird feed")) {
                            if (Inventory.contains("Pigeon cage")) {
                                if (o < 1) {
                                    talkToOmart();
                                } else {
                                    if (TOWERFENCE.distance() < 1) {
                                        SceneObject FENCE = SceneObjects.getNearest("Watchtower");

                                        if (Inventory.contains("Bird feed") && Inventory.getFirst("Bird feed").interact("Use") && !Inventory.isItemSelected()) {
                                            Time.sleepUntil(Inventory::isItemSelected, 1000);
                                        }

                                        if (Inventory.isItemSelected() && FENCE != null && FENCE.interact("Use")) {
                                            Time.sleep(500, 1500);
                                        }

                                        if (!Inventory.contains("Bird feed")) {
                                            Inventory.getFirst("Pigeon cage").interact("Open");
                                        }
                                    } else {
                                        Movement.walkTo(TOWERFENCE);
                                        sleep();
                                    }
                                }
                            } else {
                                Pickable CAGE = Pickables.getNearest("Pigeon cage");

                                if (CAGE != null && CAGE.isPositionInteractable()) {
                                    CAGE.interact("Take");
                                } else {
                                    Movement.walkTo(new Position(2618, 3325));
                                    sleep();
                                }
                            }
                        } else {
                            SceneObject CUPBOARD = SceneObjects.getNearest("Cupboard");

                            if (CUPBOARD != null) {
                                if (CUPBOARD.containsAction("Open") && CUPBOARD.interact("Open")) {
                                    CUPBOARD.interact("Search");
                                } else {
                                    if (CUPBOARD.containsAction("Search")) {
                                        CUPBOARD.interact("Search");
                                    }
                                }
                            }
                        }
                        break;
                    case 3:
                    case 4:
                        talkToOmart();
                        break;
                    case 5:
                        if (Inventory.contains("Rotten apple")) {
                            if (MOURNERHOUSEBACKYARD.contains(Players.getLocal())) {
                                SceneObject CAULDRON = SceneObjects.getNearest("Cauldron");

                                if (Inventory.contains("Rotten apple") && Inventory.getFirst("Rotten apple").interact("Use") && !Inventory.isItemSelected()) {
                                    Time.sleepUntil(Inventory::isItemSelected, 1000);
                                }

                                if (Inventory.isItemSelected() && CAULDRON != null && CAULDRON.interact("Use")) {
                                    Time.sleep(500, 1500);
                                }
                            } else {
                                SceneObject CFENCE = SceneObjects.getNearest("Fence");

                                if (CFENCE != null) {
                                    CFENCE.interact("Squeeze-through");
                                    sleep();
                                }
                            }
                        } else {
                            if (ROTTENAPPLE.distance() <= 5) {
                                Pickable ROTTENAPPLE = Pickables.getNearest("Rotten apple");

                                if (ROTTENAPPLE != null) {
                                    ROTTENAPPLE.interact("Take");
                                }
                            } else {
                                Movement.walkToRandomized(ROTTENAPPLE);
                                sleep();
                            }
                        }
                        break;
                    case 6:
                        if (EquipmentSlot.CHEST.getItem() != null && EquipmentSlot.CHEST.getItem().getName().contains("Medical")) {
                            if (Players.getLocal().getFloorLevel() != 1) {
                                if (MOURNERSTAIR.contains(Players.getLocal())) {
                                    SceneObject MSTAIRS = SceneObjects.getNearest("Staircase");

                                    if (MSTAIRS != null) {
                                        MSTAIRS.interact("Climb-up");
                                    }
                                } else {
                                    Movement.walkToRandomized(MOURNERSTAIR.getCenter());
                                    sleep();
                                }
                            } else {
                                Npc MOURNER = Npcs.getNearest("Mourner");

                                if (!Inventory.contains("key")) {
                                    if (Players.getLocal().getTargetIndex() < 0) {
                                        if (MOURNER != null) {
                                            if (MOURNER.isPositionInteractable()) {
                                                MOURNER.interact("Attack");
                                                Time.sleepUntil(() -> Players.getLocal().getTargetIndex() > 0, 1000, 2000);
                                            } else {
                                                Movement.walkToRandomized(MOURNER);
                                                sleep();
                                            }
                                        }
                                    }
                                } else {
                                    if (CRATEROOM.contains(Players.getLocal())) {
                                        if (!Inventory.contains("Distillator")) {
                                            SceneObject CRATE = SceneObjects.getNearest(2064);

                                            if (CRATE != null) {
                                                CRATE.interact("Search");
                                                sleep();
                                            }
                                        }
                                    } else {
                                        SceneObject GATE = SceneObjects.getNearest("Gate");

                                        if (Inventory.contains("Key") && Inventory.getFirst("Key").interact("Use") && !Inventory.isItemSelected()) {
                                            Time.sleepUntil(Inventory::isItemSelected, 1000);
                                        }

                                        if (Inventory.isItemSelected() && GATE != null && GATE.interact("Use")) {
                                            Time.sleep(1000, 1500);
                                        }
                                    }
                                }
                            }
                        } else {
                            if (!Inventory.contains(x -> x.getName().contains("gown"))) {
                                if (DOCTORHOUSE.contains(Players.getLocal())) {
                                    SceneObject DCUP = SceneObjects.getNearest("Cupboard");

                                    if (DCUP != null) {
                                        if (DCUP.containsAction("Open") && DCUP.interact("Open")) {
                                            DCUP.interact("Search");
                                            sleep();
                                        } else {
                                            if (DCUP.containsAction("Search")) {
                                                DCUP.interact("Search");
                                                sleep();
                                            }
                                        }
                                    }
                                } else {
                                    if (MOURNERHOUSEBACKYARD.contains(Players.getLocal())) {
                                        SceneObject CFENCE = SceneObjects.getNearest("Fence");

                                        if (CFENCE != null) {
                                            CFENCE.interact("Squeeze-through");
                                            sleep();
                                        }
                                    } else {
                                        Movement.walkTo(DOCTORHOUSE.getCenter());
                                        sleep();
                                    }
                                }
                            } else {
                                Inventory.getFirst(x -> x.getName().contains("gown")).interact("Wear");
                            }
                        }
                        break;
                    case 7:
                        if (Players.getLocal().getFloorLevel() != 0) {
                            if (Inventory.contains(x -> x.getName().contains("passage"))) {
                                Inventory.getFirst(x -> x.getName().contains("passage")).interact("Rub");
                            } else {
                                Movement.walkToRandomized(MOURNERSTAIR.getCenter());
                                sleep();
                            }
                        } else {
                            if (WESTARDY.contains(Players.getLocal())) {
                                if (FENCEJUMP.contains(Players.getLocal())) {
                                    Npc KILRON = Npcs.getNearest("Kilron");

                                    if (KILRON != null) {
                                        KILRON.interact("Talk-to");
                                    }
                                } else {
                                    Movement.walkToRandomized(FENCEJUMP.getCenter());
                                    sleep();
                                }
                            } else {
                                talkToElena();
                            }
                        }
                        break;
                    case 8:
                    case 9:
                    case 10:
                        if (WESTARDY.getCenter().distance() < CHEMISTHOUSE.getCenter().distance()) {
                            traverseToRimmington();
                        } else {
                            if (CHEMISTHOUSE.contains(Players.getLocal())) {
                                Npc CHEMIST = Npcs.getNearest("Chemist");

                                if (CHEMIST != null) {
                                    CHEMIST.interact("Talk-to");
                                    Time.sleepUntil(Dialog::isViewingChat, 500, 1500);
                                }
                            } else {
                                traverseToRimmington();
                            }
                        }
                        break;
                    case 12:
                        switch (m) {
                            case 0:
                                if (CAMPFIRE.contains(Players.getLocal())) {
                                    if (hasSB()) {
                                        dialogueOptions[6] = "broline...";
                                        Npc HOPS = Npcs.getNearest("Hops");

                                        if (HOPS != null) {
                                            HOPS.interact("Talk-to");
                                            Time.sleepUntil(Dialog::canContinue, 1000, 2000);
                                        }
                                    } else {
                                        if (hasLH()) {
                                            dialogueOptions[6] = "honey...";
                                            Npc CHANCY = Npcs.getNearest("Chancy");

                                            if (CHANCY != null) {
                                                CHANCY.interact("Talk-to");
                                                Time.sleepUntil(Dialog::canContinue, 1000, 2000);
                                            }
                                        } else {
                                            if (hasET()) {
                                                dialogueOptions[6] = "ethenea...";
                                                Npc DAVINCI = Npcs.getNearest("Da Vinci");

                                                if (DAVINCI != null) {
                                                    DAVINCI.interact("Talk-to");
                                                    Time.sleepUntil(Dialog::canContinue, 1000, 2000);
                                                }
                                            } else {
                                                m = 1;
                                            }
                                        }
                                    }
                                } else {
                                    Movement.walkToRandomized(CAMPFIRE.getCenter());
                                    sleep();
                                }
                                break;
                            case 1:
                                if (!hasBottom()) {
                                    if (Inventory.contains(428) && Inventory.contains(426)) {
                                        Inventory.getFirst(428).interact("Wear");
                                    } else {
                                        if (Bank.isOpen()) {
                                            Bank.withdraw(426, 1);
                                            Bank.withdraw(428, 1);
                                        } else {
                                            Bank.open();
                                            sleep();
                                        }
                                    }
                                } else {
                                    if (!hasTop()) {
                                        if (Inventory.contains(426)) {
                                            Inventory.getFirst(426).interact("Wear");
                                        } else {
                                            if (Bank.isOpen()) {
                                                Bank.withdraw(426, 1);
                                            } else {
                                                Bank.open();
                                                sleep();
                                            }
                                        }
                                    }
                                }

                                if (hasTop() && hasBottom()) {
                                    if (FINALLOCATION.contains(Players.getLocal())) {
                                        if (!hasET()) {
                                            if (BAR.contains(Players.getLocal())) {
                                                Npc DAVINCI = Npcs.getNearest("Da Vinci");

                                                if (DAVINCI != null) {
                                                    DAVINCI.interact("Talk-to");
                                                    Time.sleepUntil(Dialog::canContinue, 1000, 2000);
                                                }
                                            } else {
                                                Movement.walkToRandomized(BAR.getCenter());
                                                sleep();
                                            }
                                        } else {
                                            if (!hasLH()) {
                                                Npc CHANCY = Npcs.getNearest("Chancy");

                                                if (CHANCY != null) {
                                                    CHANCY.interact("Talk-to");
                                                    Time.sleepUntil(Dialog::canContinue, 1000, 2000);
                                                }
                                            } else {
                                                if (!hasSB()) {
                                                    Npc HOPS = Npcs.getNearest("Hops");

                                                    if (HOPS != null) {
                                                        HOPS.interact("Talk-to");
                                                        Time.sleepUntil(Dialog::canContinue, 1000, 2000);
                                                    }
                                                } else {
                                                    if (hasPlague()) {
                                                        if (GUIDORROOM.contains(Players.getLocal())) {
                                                            Npc GUIDOR = Npcs.getNearest("Guidor");

                                                            if (GUIDOR != null) {
                                                                GUIDOR.interact("Talk-to");
                                                                Time.sleepUntil(Dialog::canContinue, 1000, 2000);
                                                            }
                                                        } else {
                                                            if (GUIDORHOUSE.contains(Players.getLocal())) {
                                                                SceneObject DOOR = SceneObjects.getNearest("Bedroom door");

                                                                if (DOOR != null) {
                                                                    DOOR.interact("Open");
                                                                }
                                                            } else {
                                                                Movement.walkToRandomized(GUIDORHOUSE.getCenter());
                                                                sleep();
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    } else {
                                        Movement.walkToRandomized(FINALLOCATION.getCenter());
                                        sleep();
                                    }
                                }
                                break;
                        }
                        break;
                    case 14:
                        talkToElena();
                        break;
                    case 15:
                        if (KINGROOM.contains(Players.getLocal())) {
                            Npc KING = Npcs.getNearest("King Lathas");

                            if (KING != null) {
                                KING.interact("Talk-to");
                                Time.sleepUntil(Dialog::isViewingChat, 1000, 2000);
                            }
                        } else {
                            Movement.walkToRandomized(KINGROOM.getCenter());
                            sleep();
                        }
                        break;
                }
            }
        }
    }
}
